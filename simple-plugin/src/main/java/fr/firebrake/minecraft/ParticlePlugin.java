package fr.firebrake.minecraft;

import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import fr.firebrake.minecraft.command.NearCommand;

/**
 * The Class {@link ParticlePlugin}.<br>
 * The main, the all, the whole !
 */
public class ParticlePlugin extends JavaPlugin {

	@Override
	public void onEnable() {
		final ParticlePluginConfig config = new ParticlePluginConfig(this);

		// Mapping commands to Executor
		final CommandExecutor particleCmd = new NearCommand(config);
		getCommand("particle").setExecutor(particleCmd);

		// Read the config file
		config.getMainConfig().reloadConfig();
	}

	@Override
	public void onDisable() {
		getLogger().info("onDisable is called!");
	}

}
