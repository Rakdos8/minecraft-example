package fr.firebrake.minecraft;

import java.util.logging.Logger;

import fr.firebrake.minecraft.reader.MainConfigReader;
import fr.firebrake.minecraft.utils.ConfigParser;

/**
 * The Class {@link ParticlePluginConfig}.
 */
public class ParticlePluginConfig {

	private final ParticlePlugin plugin;

	/** The {@link MainConfigReader}. */
	private final MainConfigReader mainConfig;

	/**
	 * Creates a new {@link ParticlePlugin}'s config.
	 *
	 * @param plugin the {@link ParticlePlugin} plugin
	 */
	public ParticlePluginConfig(final ParticlePlugin plugin) {
		this.plugin = plugin;

		// Reading config file
		this.mainConfig = new MainConfigReader(this, new ConfigParser(plugin, "config.yml", false));
	}

	public void reloadConfig() {
		mainConfig.reloadConfig();
	}

	/**
	 * Gets the main config reader.
	 *
	 * @return the {@link MainConfigReader}
	 */
	public MainConfigReader getMainConfig() {
		return mainConfig;
	}

	public ParticlePlugin getPlugin() {
		return plugin;
	}

	public Logger getLogger() {
		return plugin.getLogger();
	}

}
