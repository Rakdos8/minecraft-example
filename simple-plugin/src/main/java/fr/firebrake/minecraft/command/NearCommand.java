package fr.firebrake.minecraft.command;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.firebrake.minecraft.ParticlePluginConfig;
import fr.firebrake.minecraft.reader.particle.SpawnParticle;
import net.md_5.bungee.api.ChatColor;

/**
 * The Class NearCommand.<br>
 * Retrieves the nearest {@link org.bukkit.Particle} spawn according to the given radius
 */
public class NearCommand implements CommandExecutor {

	/** The {@link ParticlePluginConfig}. */
	private final ParticlePluginConfig config;

	/**
	 * Creates the command NearCommand according to the config and the parent command.
	 *
	 * @param config the {@link ParticlePluginConfig}
	 */
	public NearCommand(final ParticlePluginConfig config) {
		this.config = config;
	}

	public String getDescription() {
		return "permet de connaître les particules proches";
	}

	@Override
	public boolean onCommand(
			final CommandSender sender,
			final Command command,
			final String label,
			final String[] args
	) {
		config.getLogger().info(sender.getName() + " sent a command : '/" + label + " " + String.join(" ", args) + "'");
		if (!(sender instanceof Player)) {
			config.getLogger().warning(sender.getClass().getSimpleName() + " sent a command and was unexpected");
			return false;
		}

		if (args.length < 1) {
			sender.sendMessage(formatMessage() + "Le rayon de recherche doit être spécifiée");
			return false;
		}
		final int rawRadius = Integer.parseInt(args[0]);
		final double searchRadius = Math.pow(rawRadius, 2);

		final Location startLoc = ((Player) sender).getLocation();
		final List<SpawnParticle> particles = config.getMainConfig().getSpawnParticles().stream()
				.filter(spawnParticle -> Objects.equals(startLoc.getWorld(), spawnParticle.getSpawnLocation().getWorld()))
				.filter(spawnParticle -> getDistance3d(startLoc, spawnParticle.getSpawnLocation()) < searchRadius)
				.collect(Collectors.toList());
		if (particles.isEmpty()) {
			sender.sendMessage(formatMessage() + "Aucune particule trouvée. Rayon de recherche: " + rawRadius);
			return true;
		}
		final String shownParticles = particles.stream()
				.map(SpawnParticle::getConfigName)
				.collect(Collectors.joining(", ", ChatColor.GREEN.toString(), ChatColor.RESET.toString()));
		sender.sendMessage(formatMessage() + "Vous êtes près de: " + shownParticles + ". Rayon de recherche: " + rawRadius);
		return true;
	}

	private String formatMessage() {
		return "[" + ChatColor.YELLOW + ChatColor.ITALIC + config.getPlugin().getName() + ChatColor.RESET + "] ";
	}

	/**
	 * Gets the distance in {@link Block} from the 2 given {@link Location} in 3D.
	 *
	 * @param loc1 the first {@link Location}
	 * @param loc2 the second {@link Location}
	 * @return the distance in {@link Block}, you should call {@link Math#sqrt(double)} yourself (heavy calculation)
	 */
	public static double getDistance3d(final Location loc1, final Location loc2) {
		final double x = loc2.getX() - loc1.getX();
		final double y = loc2.getY() - loc1.getY();
		final double z = loc2.getZ() - loc1.getZ();
		return x * x + y * y + z * z;
	}

}
