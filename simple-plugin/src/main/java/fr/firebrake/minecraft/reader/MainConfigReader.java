package fr.firebrake.minecraft.reader;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Particle;

import fr.firebrake.minecraft.ParticlePluginConfig;
import fr.firebrake.minecraft.reader.particle.SpawnParticle;
import fr.firebrake.minecraft.utils.ConfigParser;

/**
 * Used to read the MAIN config YAML file.
 */
public class MainConfigReader {

	private final ParticlePluginConfig config;
	private final ConfigParser configParser;

	private final Set<SpawnParticle> spawnParticles = new HashSet<>();

	/**
	 * Creates a new {@link MainConfigReader}
	 *
	 * @param config the {@link ParticlePluginConfig}
	 * @param configFile the {@link ConfigParser}
	 */
	public MainConfigReader(
			final ParticlePluginConfig config,
			final ConfigParser configFile
	) {
		this.config = config;
		this.configParser = configFile;
	}

	/**
	 * @return the unmodifiable {@link Set} of {@link SpawnParticle}
	 */
	public Set<SpawnParticle> getSpawnParticles() {
		return Collections.unmodifiableSet(spawnParticles);
	}

	/**
	 * Reloads the config file and re-parse it.
	 */
	public void reloadConfig() {
		parseConfig();
	}

	public void parseConfig() {
		// Cancel all task, then clear the cache
		spawnParticles.forEach(SpawnParticle::cancelSpawn);
		spawnParticles.clear();

		for (final String spawnName : configParser.getKeysFromPath("")) {
			final Location spawnLocation = configParser.getLocation(spawnName + ".location");
			if (spawnLocation == null) {
				continue;
			}

			final Particle particle = Particle.valueOf(configParser.getString(spawnName + ".particle", "CRIT"));

			spawnParticles.add(
					new SpawnParticle(
							config.getPlugin(),
							spawnName,
							spawnLocation,
							particle,
							configParser.getInt(spawnName + ".frequency", 0),
							configParser.getInt(spawnName + ".number", 0),
							configParser.getDouble(spawnName + ".dx", 0d),
							configParser.getDouble(spawnName + ".dy", 0d),
							configParser.getDouble(spawnName + ".dz", 0d),
							configParser.getDouble(spawnName + ".speed", 5d)
					)
			);
		}
	}

}
