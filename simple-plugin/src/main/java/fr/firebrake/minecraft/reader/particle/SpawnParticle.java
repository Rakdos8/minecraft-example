package fr.firebrake.minecraft.reader.particle;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.scheduler.BukkitTask;

import fr.firebrake.minecraft.ParticlePlugin;
import fr.firebrake.minecraft.utils.DefaultWellTask;

/**
 * Handles {@link Particle}, {@link Location}, and {@link BukkitTask}.
 */
public class SpawnParticle {

	private final DefaultWellTask spawnTask;

	private final String configName;
	private final Location spawnLocation;

	/**
	 * Creates the {@link SpawnParticle}.
	 *
	 * @param plugin the {@link ParticlePlugin}
	 * @param configName the name of the path in the YAML file
	 * @param location the {@link Location}
	 * @param particle the {@link Particle}
	 * @param number the number of {@link Particle} to spawn
	 * @param frequency the frequency of the spawn (in tick)
	 * @param dx the deviation on X
	 * @param dy the deviation on Y
	 * @param dz the deviation on Z
	 * @param speed the speed of the {@link Particle}
	 */
	public SpawnParticle(
			final ParticlePlugin plugin,
			final String configName,
			final Location location,
			final Particle particle,
			final int frequency,
			final int number,
			final double dx,
			final double dy,
			final double dz,
			final double speed

	) {
		this.configName = configName;
		this.spawnLocation = location;

		this.spawnTask = new DefaultWellTask(
				plugin,
				0,
				frequency,
				false
		) {
			@Override
			public void runTask() {
				if (location.getWorld() == null) {
					plugin.getLogger().severe("La particule " + configName + " est dans un monde == null");
					return;
				}

				location.getWorld().spawnParticle(
						particle,
						location.getX(),
						location.getY(),
						location.getZ(),
						number,
						dx,
						dy,
						dz,
						speed,
						null
				);
			}
		};
		spawnTask.registerTask();
	}

	/**
	 * @return the name of the {@link SpawnParticle} in the YAML config file
	 */
	public String getConfigName() {
		return configName;
	}

	/**
	 * @return the {@link Location} where the {@link Particle} spawn
	 */
	public Location getSpawnLocation() {
		return spawnLocation;
	}

	/**
	 * Cancels the spawn task through {@link BukkitTask#cancel()}
	 */
	public void cancelSpawn() {
		spawnTask.setCancelled(true);
	}


	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final SpawnParticle that = (SpawnParticle) o;
		return Objects.equals(configName, that.configName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(configName);
	}

}
