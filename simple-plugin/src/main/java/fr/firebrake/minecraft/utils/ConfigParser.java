package fr.firebrake.minecraft.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The Class {@link ConfigParser}.<br>
 * It allows to create any config file given from resource. It also allows to parse the file.
 */
public class ConfigParser extends YamlConfiguration {

	/** The {@link JavaPlugin} who called this YamlParser. */
	private final JavaPlugin plugin;

	/** The current YAML file to parse. */
	private final File configFile;

	/**
	 * Instantiates a new {@link ConfigParser}.<br>
	 * It will update the file from resource by default (every difference).
	 *
	 * @param plugin the {@link JavaPlugin}
	 * @param configFileName the config file name
	 */
	public ConfigParser(final JavaPlugin plugin, final String configFileName) {
		this(plugin, new File(plugin.getDataFolder(), configFileName), true);
	}

	/**
	 * Instantiates a new {@link ConfigParser}.
	 *
	 * @param plugin the {@link JavaPlugin}
	 * @param configFileName the config file name
	 * @param updateIfDiff should we update the file from the resource ?
	 */
	public ConfigParser(final JavaPlugin plugin, final String configFileName, final boolean updateIfDiff) {
		this(plugin, new File(plugin.getDataFolder(), configFileName), updateIfDiff);
	}

	/**
	 * Instantiates a new {@link ConfigParser}.
	 *
	 * @param plugin the {@link JavaPlugin}
	 * @param configFile the config {@link File}
	 * @param updateIfDiff should we update the file from the resource ?
	 */
	public ConfigParser(final JavaPlugin plugin, final File configFile, final boolean updateIfDiff) {
		this.plugin = plugin;

		// Getting the File in the plugin's folder
		this.configFile = getConfigFile(configFile);

		// Parsing the File to retrieve data
		parseConfigFile();

		// Updating the File if necessary
		if (updateIfDiff) {
			updateConfigFromResource(getFileConfigFromResource(configFile.getName()));
		}
	}

	/**
	 * Reloads the current config file.
	 */
	public void reloadConfigFile() {
		parseConfigFile();
	}

	/**
	 * Saves the current config file.
	 *
	 * @throws IOException Thrown when the given file cannot be written to for any reason.
	 */
	public void saveConfigFile() throws IOException {
		super.save(configFile);
	}

	/**
	 * Gets the config file.
	 *
	 * @return the config file
	 */
	public File getConfigFile() {
		return configFile;
	}

	/**
	 * Gets the distinct list of the keys from the given path.
	 *
	 * @param path the path as a {@link String}
	 * @return the distinct list of keys (can be empty)
	 */
	public Set<String> getKeysFromPath(final String path) {
		final Object getValue = super.get(path);
		if (getValue instanceof MemorySection) {
			return ((MemorySection) getValue).getKeys(false);
		}
		return Collections.emptySet();
	}

	/**
	 * Retrieves the Location from the String with the following pattern:
	 * <ol>
	 * <li><b>WORLD:</b> it will teleport to its spawn</li>
	 * <li><b>WORLD;X;Y;Z:</b> it will teleport to the given X, Y, and Z</li>
	 * <li><b>WORLD;X;Y;Z;YAW;PITCH:</b> it will teleport to the given X, Y, Z and will apply the yaw and pitch</li>
	 * </ol>
	 *
	 * @param path the path
	 * @return the {@link Location} (with yaw/pitch if given) or null if invalid
	 */
	@Override
	public Location getLocation(final String path) {
		// Wrong path or does not exist
		if (path.isEmpty()) {
			plugin.getLogger().warning("The location is empty for path: " + path);
			return null;
		}

		return parseLocationFromString(path, getString(path, ""));
	}

	/**
	 * Retrieves the every Location from the String with the following pattern:
	 * <ol>
	 * <li><b>WORLD:</b> it will teleport to its spawn</li>
	 * <li><b>WORLD;X;Y;Z:</b> it will teleport to the given X, Y, and Z</li>
	 * <li><b>WORLD;X;Y;Z;YAW;PITCH:</b> it will teleport to the given X, Y, Z and will apply the yaw and pitch</li>
	 * </ol>
	 *
	 * @param path the path
	 * @return the list of valid {@link Location} (with yaw/pitch if given) or {@link Collections#emptyList()} if invalid
	 */
	public List<Location> getLocationList(final String path) {
		final List<String> strLocations = getStringList(path).stream()
				.filter(str -> !str.isEmpty())
				.collect(Collectors.toList());
		// Wrong path or does not exist
		if (strLocations.isEmpty()) {
			plugin.getLogger().warning("The location is empty for path: " + path);
			return Collections.emptyList();
		}

		final List<Location> locations = new ArrayList<>();
		for (final String strLocation : strLocations) {
			final Location loc = parseLocationFromString(path, strLocation);
			if (loc != null) {
				locations.add(loc);
			}
		}
		return locations;
	}

	private Location parseLocationFromString(
			final String path,
			final String strLocation
	) {
		final int nbSemiColon = strLocation.split(";").length - 1;
		// Wrong format !
		if (nbSemiColon != 0 && nbSemiColon != 3 && nbSemiColon != 5) {
			plugin.getLogger().warning(
					"The location pattern is malformed, excepting 0, 3, or 5 semicolon. Got "
							+ nbSemiColon
							+ " ("
							+ strLocation
							+ ") for path: "
							+ path
			);
			return null;
		}

		// If no semicolon, it's the world's main spawn
		if (nbSemiColon == 0) {
			final World world = Bukkit.getWorld(strLocation);
			if (world == null) {
				plugin.getLogger().warning(
						"The world "
								+ strLocation
								+ " does not exists for path: "
								+ path
				);
				return null;
			}
			return world.getSpawnLocation();
		}

		final List<String> locArgs = Arrays.stream(strLocation.split(";"))
				.map(str -> str.replace(",", "."))
				.map(str -> str.replace(";", ""))
				.collect(Collectors.toList());
		final World world = Bukkit.getWorld(locArgs.get(0));
		if (world == null) {
			plugin.getLogger().warning(
					"The world "
							+ strLocation
							+ " does not exists for path: "
							+ path
			);
			return null;
		}
		return new Location(
				world,
				Double.parseDouble(locArgs.get(1)),
				Double.parseDouble(locArgs.get(2)),
				Double.parseDouble(locArgs.get(3))
		);
	}

	/**
	 * Gets the file from the plugin's folder. If it does not exist, it will take it from resource.
	 *
	 * @param configFile the config {@link File} to read and parse
	 * @return the {@link File} in the plugin's folder
	 */
	private File getConfigFile(final File configFile) {
		// If the file does not exist, create one from the default
		if (!configFile.exists()) {
			plugin.getLogger().log(
					Level.INFO,
					"Aucun fichier de configuration trouvé pour "
							+ configFile.getName()
							+ ", création d'un nouveau par défaut !"
			);
			final FileConfiguration tmpConfig = getFileConfigFromResource("config/" + configFile.getName());
			if (tmpConfig == null) {
				throw new IllegalStateException("Aucune ressource du nom de " + configFile.getName());
			}
			tmpConfig.options().copyDefaults(true);
			try {
				tmpConfig.save(configFile);
			} catch (final IOException ex) {
				plugin.getLogger().log(Level.SEVERE, ex.getLocalizedMessage(), ex);
			}
		}
		return configFile;
	}

	/**
	 * Parses the file into memory.
	 */
	private void parseConfigFile() {
		// Load the config file
		try {
			super.load(configFile);
		} catch (final IOException | InvalidConfigurationException ex) {
			plugin.getLogger().log(Level.SEVERE, ex.getLocalizedMessage(), ex);
		}
	}

	/**
	 * Updates the config file (in the plugin folder) from the resource one if necessary.
	 *
	 * @param resourceConfig the config file from the resource
	 * @return true if any update have been done, false otherwise
	 */
	private boolean updateConfigFromResource(
			final FileConfiguration resourceConfig
	) {
		boolean updateDone = false;
		if (resourceConfig != null) {
			final Map<String, Object> configValues = super.getValues(true);
			final Map<String, Object> resourceValues = resourceConfig.getValues(true);

			for (final Map.Entry<String, Object> entry : resourceValues.entrySet()) {
				final String curPath = entry.getKey();
				final Object value = entry.getValue();

				if (!(resourceValues.get(curPath) instanceof MemorySection) && (configValues.get(curPath) == null)) {
					updateDone = true;
					super.set(curPath, value);
					plugin.getLogger().log(
							Level.INFO,
							"Le fichier de configuration a été mis à jour avec: "
									+ curPath
									+ " = "
									+ value
					);
				}
			}

			if (updateDone) {
				try {
					super.save(configFile);
				} catch (final IOException ex) {
					plugin.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
				}
			}
		}
		return updateDone;
	}

	/**
	 * Gets the FileConfiguration (parsed) from the resource with the given path.
	 *
	 * @param path the path from the resource
	 * @return the {@link FileConfiguration} found if everything went OK, null otherwise
	 */
	private FileConfiguration getFileConfigFromResource(final String path) {
		final InputStream is = plugin.getResource(path);
		if (is != null) {
			// Closing a Closeable will release resources associated with it (InputStream in this case)
			try (final InputStreamReader reader = new InputStreamReader(is)) {
				final FileConfiguration config = new YamlConfiguration();
				config.load(reader);
				return config;
			} catch (final InvalidConfigurationException | IOException ex) {
				throw new IllegalStateException(ex.getMessage(), ex);
			}
		}
		return null;
	}

}
