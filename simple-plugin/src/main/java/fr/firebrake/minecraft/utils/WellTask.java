package fr.firebrake.minecraft.utils;

import java.util.concurrent.Callable;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

/**
 * The {@link WellTask} which is used to register tasks to Bukkit.
 */
public interface WellTask extends Runnable, Callable<Void> {

	/**
	 * Gets the {@link JavaPlugin}.
	 *
	 * @return the {@link JavaPlugin}
	 */
	public JavaPlugin getPlugin();

	/**
	 * The main method which does your magic.<br>
	 * You can not use the {@link Runnable#run()}, specific use of {@link DefaultWellTask}.
	 */
	public void runTask();

	/**
	 * The callback Bukkit thread safe.<br>
	 * You can not use the {@link Callable#call()}, specific use of {@link DefaultWellTask}.
	 */
	public default void callbackSyncWithBukkit() {
		// Meant to be override if needed
	}

	/**
	 * Gets the delay to start the task (in tick).
	 *
	 * @return the delay
	 */
	public int getDelay();

	/**
	 * Gets the period to re-call the task.
	 *
	 * @return the period, -1 if the task is called only one time
	 */
	public int getPeriod();

	/**
	 * Is the task asynchronous ?<br>
	 * Reduces lags on the server but you should not use the Bukkit API in the {@link Runnable#run()} !
	 *
	 * @return true if the task is asynchronous, false otherwise
	 */
	public boolean isAsynchronous();

	/**
	 * Sets the {@link BukkitTask} of the {@link WellTask}.
	 *
	 * @param bukkitTask the {@link BukkitTask}
	 */
	public void setBukkitTask(final BukkitTask bukkitTask);

	/**
	 * Retrieves the {@link BukkitTask} linked to
	 * this {@link WellTask}.
	 *
	 * @return the {@link BukkitTask}
	 */
	public BukkitTask getBukkitTask();

}
