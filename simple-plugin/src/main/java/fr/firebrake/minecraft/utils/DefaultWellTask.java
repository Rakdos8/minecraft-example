package fr.firebrake.minecraft.utils;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

/**
 * The default {@link WellTask}.
 */
public abstract class DefaultWellTask implements WellTask, Cancellable {

	/** The {@link JavaPlugin}. */
	protected final JavaPlugin plugin;

	/** The delay of the task (in ticks). */
	protected final int delay;

	/** The period of the task (in ticks). */
	protected final int period;

	/** Is the task asynchronous ? */
	protected final boolean asynchronous;

	/** The BukkitTask from the API. */
	protected BukkitTask bukkitTask;

	/**
	 * Creates a synchronous {@link DefaultWellTask} with no delay.
	 *
	 * @param plugin the {@link JavaPlugin}
	 * @param period the period -1 if only called once
	 */
	public DefaultWellTask(
			final JavaPlugin plugin,
			final int period
	) {
		this(plugin, 0, period, false);
	}

	/**
	 * Creates the {@link DefaultWellTask} according to the mandatory field.
	 *
	 * @param plugin the {@link JavaPlugin}
	 * @param delay the delay
	 * @param period the period -1 if only called once
	 * @param asynchronous is the task asynchronous ?
	 */
	public DefaultWellTask(
			final JavaPlugin plugin,
			final int delay,
			final int period,
			final boolean asynchronous
	) {
		this.plugin = plugin;

		this.delay = delay;
		this.period = period;
		this.asynchronous = asynchronous;
	}

	@Override
	public JavaPlugin getPlugin() {
		return plugin;
	}

	@Override
	public void run() {
		try {
			runTask();

			// Ask Bukkit to run the #call method synchronously
			Bukkit.getScheduler().callSyncMethod(plugin, this);
		} catch (final Exception ex) {
			// Handle uncaught Exceptions
			plugin.getLogger().log(
					Level.SEVERE,
					"Happened in WellTask: " + ex.getMessage(),
					ex
			);
		}
	}

	@Override
	public Void call() {
		try {
			callbackSyncWithBukkit();
		} catch (final Exception ex) {
			// Handle uncaught Exceptions
			plugin.getLogger().log(
					Level.SEVERE,
					"Happened in WellTask: " + ex.getMessage(),
					ex
			);
		}
		return null;
	}

	@Override
	public int getDelay() {
		return delay;
	}

	@Override
	public int getPeriod() {
		return period;
	}

	@Override
	public boolean isAsynchronous() {
		return asynchronous;
	}

	@Override
	public void setBukkitTask(final BukkitTask bukkitTask) {
		this.bukkitTask = bukkitTask;
	}

	@Override
	public BukkitTask getBukkitTask() {
		return bukkitTask;
	}

	@Override
	public boolean isCancelled() {
		return bukkitTask != null && bukkitTask.isCancelled();
	}

	@Override
	public void setCancelled(final boolean cancel) {
		if (cancel && bukkitTask != null) {
			bukkitTask.cancel();
		}
	}

	public BukkitTask registerTask() {
		if (!this.isAsynchronous()) {
			if (this.getDelay() > 0) {
				this.bukkitTask = this.getPeriod() <= 0
						? Bukkit.getScheduler().runTaskLater(plugin, this, this.getDelay())
						: Bukkit.getScheduler().runTaskTimer(plugin, this, this.getDelay(), this.getPeriod());
			} else {
				this.bukkitTask = this.getPeriod() <= 0
						? Bukkit.getScheduler().runTask(plugin, this)
						: Bukkit.getScheduler().runTaskTimer(plugin, this, this.getDelay(), this.getPeriod());
			}
		} else {
			if (this.getDelay() > 0) {
				this.bukkitTask = this.getPeriod() <= 0
						? Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, this, this.getDelay())
						: Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this, this.getDelay(), this.getPeriod());
			} else {
				this.bukkitTask = this.getPeriod() <= 0
						? Bukkit.getScheduler().runTaskAsynchronously(plugin, this)
						: Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this, this.getDelay(), this.getPeriod());
			}
		}
		this.setBukkitTask(this.bukkitTask);
		return this.bukkitTask;
	}
}
