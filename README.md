# minecraft-example

## Introduction

Pour les besoins de ce repo, j'ai mis tous les éléments en son sein sauf quelques binaires :
1. [Maven](https://maven.apache.org/download.cgi)
  - Idéalement à installer dans un dossier comme `C:\dev`
  - Créer une variable d'environnement `M2_HOME`
  - Modifier la variable `PATH` et rajouter `%M2_HOME%\bin`
2. [OpenJDK 21](https://learn.microsoft.com/fr-fr/java/openjdk/download#openjdk-21)
  - Télécharger l'archive et décompresser dans le dossier [jdk21.0.1+12](./jdk21.0.1+12)
  - Créer une variable d'environnement `JAVA_HOME` pour pointer dans le [dossier](./jdk21.0.1+12) 
  - Modifier la variable `PATH` et rajouter `%JAVA_HOME%\bin`
3. [Minecraft](https://aka.ms/minecraftClientWindows)
  - Bien entendu, il faut un compte, si jamais tu n'as pas : [En version standard, ça suffira](https://www.minecraft.net/fr-fr/store/minecraft-java-bedrock-edition-pc)

Le serveur Minecraft utilisé ici est une version 1.20.4 et est sous `Spigot`. Il existe plusieurs versions de serveur différentes.
On peut comparer ça aux distributions Linux.

En termes d'architecture de dossier, je pense que c'est assez parlant :
- [jdk21.0.1+12](./jdk21.0.1+12) : JDK pour compiler et lancer le serveur Spigot
- [mc-server](./mc-server) : les fichiers permettant de lancer le serveur Minecraft
- [simple-plugin](./simple-plugin) : le projet Maven avec un plugin d'exemple

## Liens utiles

1. [Maven](https://www.youtube.com/watch?v=Xatr8AZLOsE) : vidéo qui montre l'installation, la configuration, et l'exploitation d'un projet simple (incluant des tests unitaires), ainsi que l'imbrication en module
2. [YouTube](https://www.youtube.com/watch?v=_HZVWuRYzjY&list=PL_cUvD4qzbkwg80GfPvyCkqCHsjzKSaW3&index=1) : une petite série qui explique comment faire des plugins pour Minecraft
3. [Spigot Wiki](https://www.spigotmc.org/wiki/spigot/) : Wiki officiel de l'éditeur du serveur communautaire
4. [Javadoc](https://hub.spigotmc.org/javadocs/spigot/) : Javadoc officielle de l'API du serveur Minecraft

## Une fois l'installation finie

Une fois les outils installés et configuré, la commande `mvn clean install` dans le dossier [simple-plugin](./simple-plugin) permettra de placer le plugin compilé dans le dossier [plugins](./mc-server/plugins) de Minecraft.\
Garder ce terminal dans un coin, car il servira beaucoup :) !

Depuis le dossier [mc-server](./mc-server), exécuter le fichier [start.bat](./mc-server/start.bat). C'est ce que l'on apelle la "console"\
Sur la console, il devrait y avoir beaucoup de log et d'information. N'hésite pas à regarder ce qu'il s'y passe et mettre ça sur un écran dédié pour voir les nouveaux logs.

À partir de ce moment, tu peux lancer Minecraft depuis son Launcher en vers 1.20.4\
Une fois le jeu démarré (et configuré), sélectionné "Multijoueur" puis "Ajouter un server" avec les informations suivantes :
- Server Name : `local`
- Server Address : `127.0.0.1`\
Par défaut et par mesure de sureté, le serveur n'est joignable qu'en local

Le logo du serveur devrait apparaître ainsi qu'un compteur de joueur connecté (`0/20`)

Si tu te connectes, tu devrais arriver sur une île de sable. Si ce n'est pas le cas, dirige-toi aux coordonnées suivantes : 
- X: -9
- Y: 87
- Z: 5

Ces coordonnées sont affichées dans le jeu en appuyant sur la touche `F3`. Une fois arrivée, il devrait y avoir de la poussière voler : **c'est le plugin qui l'a généré** !\
Pour te déplacer en sûreté, tu peux taper la commande `/gamemode creative` et en faisant un double saut ton personnage volera ce qui facilitera le déplacement. Pour descendre en altitude, il faut se baisser (sneak).

Si le jeu ne permet pas d'exécuter la commande, il faudra depuis la console taper la commande `op <NOM_DU_JOUEUR>` pour te mettre opérateur (admin).

## What next ?

Prendre le temps de regarder le code du plugin proposé :)\
C'est du vieux code d'un plugin bien plus complexe et gros, mais ça n'empêche qu'il donne une base pour mettre ses mains dedans. Il n'a rien d'extraordinaire (au contraire) mais cela fera partie des challenges !

À noter que ce plugin propose une commande `/particle <int:rayon>` pour connaitre les particules proches de soi (selon un rayon) :
- Voir [l'implémantation de la commande](./simple-plugin/src/main/java/fr/firebrake/minecraft/command/NearCommand.java)
- Voir [la définition de la commande à Spigot](./simple-plugin/src/main/resources/plugin.yml)
- Voir [l'affectation de la commande avec son implémentation](./simple-plugin/src/main/java/fr/firebrake/minecraft/ParticlePlugin.java)

Parlant de challenge, voici qui devrait t'occuper à faire. Du plus "simple" au plus complexe :

1. Créer une commande `entity-create` qui doit faire apparaitre une [Entity](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Entity.html) sur le joueur qui exécute la commande

<details>
   <summary>Cheat code(s)</summary>

1. Tu peux copier la commande déjà existante `NearCommand` pour faire la nouvelle commande.
2. Tout se joue dans la méthode `onCommand`
3. Une fois la position du joueur trouvée, tu peux obtenir le monde dans lequel il évolue
4. C'est le monde qui gère les [entités affichées](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/RegionAccessor.html#spawnEntity(org.bukkit.Location,org.bukkit.entity.EntityType))
</details>

2. Modifier la commande `entity-create` pour rajouter un 2e paramètre qui sera le nombre d'entité à faire apparaître

<details>
   <summary>Cheat code(s)</summary>

1. Une boucle `for` fait très bien le travail ;) !
</details>

3. Créer une commande `entity-near` qui doit lister les entités proches du joueur à l'image de la recherche des particules selon un rayon

<details>
   <summary>Cheat code(s)</summary>

1. Une fois la position du joueur trouvée, tu peux obtenir le monde dans lequel il évolue
2. C'est le monde qui gère les [entités affichées](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/World.html#getNearbyEntities(org.bukkit.Location,double,double,double))
</details>

4. Créer une commande `entity-kill` qui doit lister les entités proches du joueur et les tuer

<details>
   <summary>Cheat code(s)</summary>

1. Une fois qu'on a la liste des entités, ça devient quand même simple
2. [Entity#remove()](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Entity.html#remove())
</details>

5. Modifier la commande `entity-create` pour permettre d'avoir une auto-complétion au tab pour proposer les types automatiquement selon les premières lettres

<details>
   <summary>Cheat code(s)</summary>

1. C'est au même niveau que l'affectation de la commande avec son implémentation
2. Dans [PluginCommand](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/command/PluginCommand.html)
3. Les bons développeurs mettront plusieurs interfaces sur une même classe
</details>

6. Maintenant que tu as plein de code dupliqué, profites-en pour créer des méthodes utilitaires prêtes à l'emploi
  - Dans l'écosystème Java, on utilise beaucoup le static pour ça

7. Si tu veux tâter le polymorphisme, la meilleure façon est de modifier la commande `entity-near` pour ne compter que le type donné
  - Tout ce qui bouge dans Minecraft est une entité listée dans des sous-catégories : Animals, Mobs (Monstres)
  - Ces sous-catégories ont encore des spécificités : si les animaux peuvent être domptés, si les monstres peuvent donner des récompenses, ...
  - Profites-en pour regarder de plus prêt le polymorphisme de [Skeleton](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Skeleton.html), et de [Cow](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Cow.html) (et des autres)

8. Pour les plus hardy, il y a un système d'[Event](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/Event.html) dans Minecraft qui peut aller très loin (et même annuler des actions)
  - Ne pas oublier de bind le Listener à [l'API Minecraft](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/plugin/PluginManager.html#registerEvents(org.bukkit.event.Listener,org.bukkit.plugin.Plugin)) !
  - Créer un [Listener](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/Listener.html) dédié à la [détection du chat](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/player/AsyncPlayerChatEvent.html)
    - Rapide et simple pour voir si ton implémentation fonctionne !
  - Créer un [Listener](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/Listener.html) dédié à [l'apparition d'entité](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/entity/EntitySpawnEvent.html)
    - Tu peux annuler l'apparition de certains monstres, ou non et tester ça grâce à la commande `entity-spawn`

<details>
   <summary>Cheat code(s)</summary>

1. Le cheat code ultime est là : https://www.spigotmc.org/wiki/using-the-event-api/
</details>

9. Fais-toi une commande pour te TP selon le X, Y, Z

10. Fais-toi une commande pour être invincible
  - Plus qu'une commande, un [Event](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/entity/EntityDamageByEntityEvent.html) qui va vérifier que celui qui reçoit les dégâts est un joueur et que c'est bien toi (via le pseudo)
